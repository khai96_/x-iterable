
# Extensible Iterable class utilities
Sum up 6 packages: [x-iterable-base](https://www.npmjs.com/package/x-iterable-base), [concat-iterable](https://www.npmjs.com/package/concat-iterable), [deep-iterable](https://www.npmjs.com/package/deep-iterable), [parallel-iterable](https://www.npmjs.com/package/parallel-iterable), [product-iterable](https://www.npmjs.com/package/product-iterable), [spread-iterable](https://www.npmjs.com/package/spread-iterable)

## Requirements

 * Node >= 6.0.0

## Import

```javascript
var {
    XIterable,
    Root,
    ConcatIterable,
    DeepIterable,
    ParallelIterable,
    ProductIterable,
    SpreadIterable
} = require('x-iterable');
```

## License

[MIT](https://github.com/ksxnodemodules/my-licenses/blob/master/MIT.md) © [Hoàng Văn Khải](https://github.com/KSXGitHub)
